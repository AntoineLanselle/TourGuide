# Tour Guide
TourGuide is a Spring Boot application and the centerpiece of TripMaster applications.
It enables users to see which tourist attractions are nearby tourist attractions,
and to obtain discounts on hotel stays and as well as on tickets for various shows.

## Technical
- Java 17.0.7
- Spring Boot 3.1.0
- Gradle 7.6.1
- JUnit
- JaCoCo

## Functionalities
- Get information on tourist attractions.
- Propose personalized offers to users (price, trip duration, etc.).
- Suggested attractions based on the user's geolocation.
- Reward points for visiting tourist attractions.

## Installation
1. Clone the repository with git
   ```sh
   git clone https://gitlab.com/AntoineLanselle/TourGuide.git
   ```
2. Run the application on your IDE

